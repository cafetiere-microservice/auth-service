package com.cafetiere.auth.service.controller;

import com.cafetiere.auth.service.model.Account;
import com.cafetiere.auth.service.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping(path = "/auth")
public class AuthController {
    @Autowired
    private AuthService authService;

    @PostMapping(path = "/register", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Account> registerAccount(@RequestBody Account account) throws ExecutionException, InterruptedException {
        CompletableFuture<Account> response = authService.register(account);
        return ResponseEntity.ok(response.get());
    }
}
