package com.cafetiere.auth.service.controller;

import com.cafetiere.auth.service.model.Account;
import com.cafetiere.auth.service.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping(path = "/user")
public class UserController {
    @Autowired
    private AuthService authService;

    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Account> getAccount(@PathVariable(value = "id") int id) throws ExecutionException, InterruptedException {
        CompletableFuture<Account> response = authService.getAccountById(id);
        return ResponseEntity.ok(response.get());
    }

    @GetMapping(path = "/username/{username}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Account> getAccountByUsername(@PathVariable(value = "username") String username) throws ExecutionException, InterruptedException {
        CompletableFuture<Account> response = authService.getAccountByUsername(username);
        return ResponseEntity.ok(response.get());
    }
}
