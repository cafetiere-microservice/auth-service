package com.cafetiere.auth.service.repository;

import com.cafetiere.auth.service.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthRepository extends JpaRepository<Account, Integer> {
    Account findAccountByUsername(String username);

    Account findAccountById(int id);
}
