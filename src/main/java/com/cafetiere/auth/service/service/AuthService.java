package com.cafetiere.auth.service.service;

import com.cafetiere.auth.service.model.Account;

import java.util.concurrent.CompletableFuture;

public interface AuthService {
    CompletableFuture<Account> register(Account account);

    CompletableFuture<Account> getAccountById(int id);

    CompletableFuture<Account> getAccountByUsername(String username);
}
