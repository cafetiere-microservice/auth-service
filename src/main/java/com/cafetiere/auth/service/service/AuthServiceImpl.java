package com.cafetiere.auth.service.service;

import com.cafetiere.auth.service.model.Account;
import com.cafetiere.auth.service.repository.AuthRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public class AuthServiceImpl implements AuthService{
    @Autowired
    private AuthRepository authRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    @Async
    public CompletableFuture<Account> register(Account account) {
        Account byUsername = authRepository.findAccountByUsername(account.getUsername());
        if (byUsername != null) {
            throw new RuntimeException("Username already exist, use another username.");
        }
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        account.setRole("1");
        authRepository.save(account);
        return CompletableFuture.completedFuture(account);
    }

    @Override
    @Async
    public CompletableFuture<Account> getAccountById(int id) {
        Account result = authRepository.findAccountById(id);
        return CompletableFuture.completedFuture(result);
    }

    @Override
    @Async
    public CompletableFuture<Account> getAccountByUsername(String username) {
        Account result = authRepository.findAccountByUsername(username);
        return CompletableFuture.completedFuture(result);
    }
}
