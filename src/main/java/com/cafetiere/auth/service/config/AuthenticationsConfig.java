package com.cafetiere.auth.service.config;

import jdk.nashorn.internal.ir.annotations.Ignore;

public class AuthenticationsConfig {
    public static final String SECRET = "Java_to_Dev_Secret";
    public static final long EXPIRATION_TIME = 10800000; // 3 Hour
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/auth/register";
    public static final String USER_URL = "/user/**";
}
