package com.cafetiere.auth.service.controller;

import com.cafetiere.auth.service.model.Account;
import com.cafetiere.auth.service.service.AuthService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import java.util.concurrent.CompletableFuture;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = UserController.class)
public class UserControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private AuthService authService;

    private CompletableFuture<Account> result;

    private Account account;

    @BeforeEach
    public void setUp(){
        account = new Account("nofaldi", "atmam", "nofame@email.com", "nofamex", "passwd");
        account.setId(1);
        result = CompletableFuture.completedFuture(account);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testGetAccount() throws Exception {
        when(authService.getAccountById(1)).thenReturn(result);
        mvc.perform(get("/user/1")
                .contentType(MediaType.APPLICATION_JSON).content(String.valueOf(account)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.username").value("nofamex"));

    }

    @Test
    public void testGetAccountByUsername() throws Exception {
        when(authService.getAccountByUsername("nofamex")).thenReturn(result);
        mvc.perform(get("/user/username/nofamex")
                .contentType(MediaType.APPLICATION_JSON).content(String.valueOf(account)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.username").value("nofamex"));

    }
}
