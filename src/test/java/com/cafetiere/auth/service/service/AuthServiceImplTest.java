package com.cafetiere.auth.service.service;

import com.cafetiere.auth.service.model.Account;
import com.cafetiere.auth.service.repository.AuthRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthServiceImplTest {
    @Mock
    private AuthRepository authRepository;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @InjectMocks
    private AuthServiceImpl authService;

    private Account account;

    @BeforeEach
    public void setUp() {
        account = new Account("nofaldi", "atmam", "nofame@email.com", "nofamex", "passwd");
        account.setId(1);
    }

    @Test
    public void testGetAccounyById() throws Exception {
        when(authRepository.findAccountById(account.getId())).thenReturn(account);
        CompletableFuture<Account> acc = acc = authService.getAccountById(1);
        assertEquals(account.getUsername(), acc.get().getUsername());
    }

    @Test
    public void testGetAccountByUsername() throws Exception {
        when(authRepository.findAccountByUsername(account.getUsername())).thenReturn(account);
        CompletableFuture<Account> acc = acc = authService.getAccountByUsername("nofamex");
        assertEquals(account.getUsername(), acc.get().getUsername());
    }

    @Test
    public void testRegisterExistedUsername() {
        when(authRepository.findAccountByUsername(account.getUsername())).thenReturn(account);
        assertThrows(RuntimeException.class, () -> {
            authService.register(account);
        });
    }

    @Test
    public void testRegisterAccount() throws Exception {
        Account newAccount = new Account("test", "test2", "test@gmail.com", "tester", "testpw");
        CompletableFuture<Account> acc = authService.register(newAccount);
        assertEquals(newAccount.getUsername(), acc.get().getUsername());
    }
}

