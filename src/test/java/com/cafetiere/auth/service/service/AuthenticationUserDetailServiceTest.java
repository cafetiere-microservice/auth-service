package com.cafetiere.auth.service.service;

import com.cafetiere.auth.service.model.Account;
import com.cafetiere.auth.service.repository.AuthRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthenticationUserDetailServiceTest {
    @Mock
    private AuthRepository authRepository;

    @InjectMocks
    private AuthenticationUserDetailService authenticationUserDetailService;

    private Account account;

    @BeforeEach
    public void setUp() {
        account = new Account("nofaldi", "atmam", "nofame@email.com", "nofamex", "passwd");
        account.setId(1);
    }

    @Test
    public void testLoadByUsernameNotFound() {
        when(authRepository.findAccountByUsername("nofamlo")).thenReturn(null);
        assertThrows(UsernameNotFoundException.class, () -> {
            authenticationUserDetailService.loadUserByUsername("nofamlo");
        });
    }

    @Test
    public void testLoadByUsernameFound() {
        when(authRepository.findAccountByUsername(account.getUsername())).thenReturn(account);
        UserDetails user = authenticationUserDetailService.loadUserByUsername(account.getUsername());
        assertEquals(user.getUsername(), account.getUsername());
    }

}

